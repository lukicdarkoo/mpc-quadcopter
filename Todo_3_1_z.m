clear
clc

Ts = 1/5;
quad = Quad(Ts);
[xs, us] = quad.trim();
sys = quad.linearize(xs, us);
[sys_x, sys_y, sys_z, sys_yaw] = quad.decompose(sys, xs, us);
mpc_z = MPC_Control_z(sys_z, Ts);

x(:,1) = [0; 2];
i = 1;
while norm(x(:,end)) > 1e-3
    u(i) = mpc_z.get_u(x(:, i));
    x(:,i+1) = mpc_z.A*x(:,i) + mpc_z.B*u(i);
    i = i + 1;
end

figure
subplot(3,1,1)
plot(x(2,:), 'o-')
ylabel('z [m]')
xlabel('Step #')

subplot(3,1,2)
plot(x(1,:), 'o-')
ylabel('z_{dot} [m/s]')
xlabel('Step #')

subplot(3,1,3)
plot(u, 'o-')
ylabel('F')
xlabel('Step #')