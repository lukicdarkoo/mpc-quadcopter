clear
clc

Ts = 1/5;
quad = Quad(Ts);
[xs, us] = quad.trim();
sys = quad.linearize(xs, us);
[sys_x, sys_y, sys_z, sys_yaw] = quad.decompose(sys, xs, us);
mpc_y = MPC_Control_x(sys_y, Ts);


x(:,1) = [0; 0; 0; 0.2];
i = 1;
while norm(x(:,end)) > 1e-3
    u(i) = mpc_y.get_u(x(:, i));
    x(:,i+1) = mpc_y.A*x(:,i) + mpc_y.B*u(i);
    i = i + 1;
end

figure
subplot(5,1,1)
plot(x(1,:), 'o-')
ylabel('Velocity pitch')
xlabel('Step #')

subplot(5,1,2)
plot(x(2,:), 'o-')
ylabel('Pitch')
xlabel('Step #')

subplot(5,1,3)
plot(x(3,:), 'o-')
ylabel('Velocity X')
xlabel('Step #')

subplot(5,1,4)
plot(x(4,:), 'o-')
ylabel('X')
xlabel('Step #')

subplot(5,1,5)
plot(u, 'o-')
ylabel('u2')
xlabel('Step #')