quad = Quad();
[xs,us] = quad.trim(); % Compute steady−state for which 0 = f(xs,us)

sys = quad.linearize(xs, us); % Linearize the nonlinear model


systransformed = sys * inv(quad.T);
systransformed

systransformed.A * [ 0; 0; 0; 0; 0; 0; 0; 0; 1; 0; 0; 1 ] + systransformed.B * [ 1; 0; 0; 0 ];

[sys_x, sys_y, sys_z, sys_yaw] = quad.decompose(sys, xs, us)