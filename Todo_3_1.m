clear
clc

Ts = 1/5;
quad = Quad(Ts);
[xs, us] = quad.trim();
sys = quad.linearize(xs, us);
[sys_x, sys_y, sys_z, sys_yaw] = quad.decompose(sys, xs, us);
mpc_z = MPC_Control_z(sys_z, Ts);
mpc_x = MPC_Control_x(sys_x, Ts);
mpc_y = MPC_Control_y(sys_y, Ts);
mpc_yaw = MPC_Control_yaw(sys_yaw, Ts);


x(:,1) = [0;0;0;%omega : roll_vel pith_vel yaw_vel
    0;0;pi/4;%theta : roll pitch yaw
    0;0;0;%vel : x_vel y_vel z_vel
    2;2;2%pos : x y z
    ];
u(:,1) = [ 0; 0; 0; 0 ];
i = 1;


while norm(x(:,end)) > 1e-3
    u(1,i) = mpc_z.get_u(x([9,12], i));
    u(2,i) = mpc_y.get_u(x([1,4,8,11], i));
    u(3,i) = mpc_x.get_u(x([2,5,7,10], i));
    u(4,i) = mpc_yaw.get_u(x([3,6], i));
    
    x([9,12],i+1) = mpc_z.A*x([9,12],i) + mpc_z.B*u(1,i);
    x([1,4,8,11],i+1) = mpc_y.A*x([1,4,8,11],i) + mpc_y.B*u(2,i);
    x([2,5,7,10],i+1) = mpc_x.A*x([2,5,7,10],i) + mpc_x.B*u(3,i);
    x([3,6],i+1) = mpc_yaw.A*x([3,6],i) + mpc_yaw.B*u(4,i);
    
    quad.plot_point(x(:, i), u(:, i));
    
    i = i + 1;
end

time_x = (0:length(x(2,:)) - 1) * Ts;
figure
subplot(2,2,1)
plot(time_x, x(4,:), 'x-')
hold
plot(time_x, x(5,:), 'o-')
plot(time_x, x(6,:), '.-')
xlabel('Time [s]')
ylabel('Angles [rad]')
legend({'Roll', 'Pitch', 'Yaw'})

subplot(2,2,2)
plot(time_x, x(1,:), 'x-')
hold
plot(time_x, x(2,:), 'o-')
plot(time_x, x(3,:), '.-')
xlabel('Time [s]')
ylabel('Angle speeds [rad/s]')
legend({'Roll', 'Pitch', 'Yaw'})

subplot(2,2,3)
plot(time_x, x(10,:), 'x-')
hold
plot(time_x, x(11,:), 'o-')
plot(time_x, x(12,:), '.-')
xlabel('Time [s]')
ylabel('Position [m]')
legend({'X', 'Y', 'Z'})

subplot(2,2,4)
plot(time_x, x(7,:), 'x-')
hold
plot(time_x, x(8,:), 'o-')
plot(time_x, x(9,:), '.-')
xlabel('Time [s]')
ylabel('Speed [m/s]')
legend({'X', 'Y', 'Z'})